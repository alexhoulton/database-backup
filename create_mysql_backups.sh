#!/bin/bash
set -x

BACKUP_DIR=/var/backups/mariadb
#ensure directory exists
mkdir -p $BACKUP_DIR

#build the date string
timestamp=$(date +%Y%m%d-%H%M%S)

#dump the database
mysqldump -u root --all-databases | gzip > $BACKUP_DIR/backup-$timestamp.sql.gz

#create latest.txt
echo $timestamp > $BACKUP_DIR/latest.txt

#nuke old backups
find $BACKUP_DIR -mtime +30 -exec rm {} \;
