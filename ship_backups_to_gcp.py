from google.cloud import storage
import os
os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = '/root/.gcp/alexhoulton-site-auth.json'
backup_dir = "/var/backups/mariadb/"
latest_filename = "latest.txt"

client = storage.Client()
bucket = client.get_bucket('alexhoulton-database-backups')

with open(backup_dir + latest_filename) as f:
    latest_backup_timestamp = f.read()

print(latest_backup_timestamp)

latest_file = bucket.blob(latest_filename)
latest_file.upload_from_string(latest_backup_timestamp)

latest_backup_filename = 'backup-' + latest_backup_timestamp.rstrip() + '.sql.gz'

print(latest_backup_filename)

latest_backup = bucket.blob(latest_backup_filename)
with open(backup_dir + latest_backup_filename, "rb") as backup:
    latest_backup.upload_from_file(backup)
